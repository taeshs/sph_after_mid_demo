// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ClientSocket.h"
#include "GameFramework/Actor.h"
#include "IPContainer.generated.h"


USTRUCT(BlueprintType)
struct Fint4 {
	GENERATED_BODY()
	int a;
	int b;
	int c;
	int d;
};

UCLASS()
class SP_DEMO_API AIPContainer : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AIPContainer();

	UFUNCTION(BlueprintCallable)
		Fint4 GetIP();
	UFUNCTION(BlueprintCallable)
		void SetIP(int a, int b, int c, int d);
	//UPROPERTY(EditAnywhere, BlueprintReadWrite)
		//int ip1;

	UFUNCTION(BlueprintCallable)
		void Is_SetIp();

protected:
	// Called when the game starts or when spawned
	//virtual void BeginPlay() override;

public:	
	// Called every frame
	//virtual void Tick(float DeltaTime) override;

};

Fint4 GetContainedIp();