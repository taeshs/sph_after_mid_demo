// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ClientSocket.h"
#include "GameFramework/Actor.h"
#include "DoorBlueprint.generated.h"

UCLASS()
class SP_DEMO_API ADoorBlueprint : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ADoorBlueprint();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool is_push;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool closet_is_push;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool drawer_is_push;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool refriger_is_push;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool dryer_is_push;

protected:
	// Called when the game starts or when spawned
	//virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
