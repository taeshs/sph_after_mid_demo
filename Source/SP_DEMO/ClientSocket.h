#pragma once


// winsock2 사용을 위해 아래 코멘트 추가
#include <iostream>
#include <string>
#include <map>
#include <WS2tcpip.h>
#include <MSWSock.h>
#include "protocol.h"

using namespace std;

#pragma comment(lib, "Ws2_32.lib")
#pragma comment(lib,"MSWSock.lib")

#define SERVER_IP		"127.0.0.1"



const char* IpAddress = NULL;

bool Init(SOCKET& hSock);
bool is_setip = false;
bool bIsConnected = false;

bool logined[4] = {};
int ided =0;

bool monster[4] = {};
int monsterided = 0;

float monster_x[4] = {};
float monster_y[4] = {};
float monster_z[4] = {};

int npc_p_id = 0;
int cplayer_id = 0;

int moveided = 0;
int cmove_npcid = 0;
int deadided = 0;
int is_move = false;
int npc_is_move = false;
bool is_destroy = false;

bool cis_push = false;
bool cis_closet_push = false;
bool cis_drawer_push = false;
bool cis_refriger_push = false;
bool cis_dryer_push = false;

bool l2cb_push = false;
bool otherobject_push = false;
bool box_push = false;
bool ai_push = false;

bool cs2c_door_push = false;
bool cs2c_key_visible = false;
bool cs2c_otherobject_push = false;
bool cs2c_box_push = false;
bool cs2c_ai_push = false;

bool cis_close_keypad = false;
bool cget_close_keypad = false;

bool cis_close_keycard = false;
bool cget_close_keycard = false;

bool cis_open_escape = false;
bool cget_open_escape = false;

bool testnode = false;

char testtext[50] = "";

float cactorx[4] = {};
float cactory[4] = {};
float cactorz[4] = {};
float cactoryaw[4] = {};
float cactorpit[4] = {};
float cactorroll[4] = {};
float cactorvx[4] = {};
float cactorvy[4] = {};
float cactorvz[4] = {};

bool cflash[4] = {};
bool cis_close_door[21] = {};
bool getis_close_door[21] = {};

bool cis_close_closet_left[6] = {};
bool getis_close_closet_left[6] = {};

bool cis_close_closet_right[6] = {};
bool getis_close_closet_right[6] = {};

bool cis_close_drawer[8] = {};
bool getis_close_drawer[8] = {};

bool cis_close_refriger[8] = {};
bool getis_close_refriger[8] = {};

bool cis_close_dryer[2] = {};
bool getis_close_dryer[2] = {};

bool cis_close_lockbox = false;
bool getis_close_lockbox = false;

bool clobby = false;
bool cstatus = false;
bool chost[4] = {};
bool cready[4] = {};
bool cstart[4] = {};
bool cis_connect[4] = {};

int s2c_ekey = 0;
int s2c_obkey = 0;
bool ckey = false;

float firstf_x=0, firstf_y=0, firstf_z=0;
float bf_x=0, bf_y=0, bf_z=0;
float secf_x=0, secf_y=0, secf_z=0;
bool key_init = false;

bool cget_escapekey[3] = {};
bool s2c_get_escapekey[3] = {};

bool cget_objectkey[11] = {};
bool s2c_get_objectkey[11] = {};

bool ckey_push = false;

bool cotherobject_open = false;
bool cis_open_coffin = false;
bool cget_open_coffin = false;

bool cis_open_board = false;
bool cget_open_board = false;

bool cis_open_safedoor = false;
bool cget_open_safedoor = false;

bool cget_robot = false;
bool cset_visible_robot = false;

bool cget_baby = false;
bool cset_visible_baby = false;

bool cbox_move = false;
bool cAI_move = false;

float box_x = 0, box_y = 0, box_z = 0;
float getbox_x = 0, getbox_y = 0, getbox_z = 0;

float mug_x[2] = {}; 
float mug_y[2] = {};
float mug_z[2] = {};
float mug_rot_x[2] = {};
float mug_rot_y[2] = {}; 
float mug_rot_z[2] = {};
float mug_vx[2] = {};
float mug_vy[2] = {};
float mug_vz[2] = {};

float getmug_x[2] = {};
float getmug_y[2] = {};
float getmug_z[2] = {};
float getmug_yaw[2] = {};
float getmug_pitch[2] = {};
float getmug_roll[2] = {};
float getmug_vx[2] = {};
float getmug_vy[2] = {};
float getmug_vz[2] = {};

