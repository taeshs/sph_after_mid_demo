// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ClientSocket.h"
#include "CoreMinimal.h"
#include "Engine/LevelScriptActor.h"
#include "SPLevelScriptActor.generated.h"

/**
 * 
 */
UCLASS()
class SP_DEMO_API ASPLevelScriptActor : public ALevelScriptActor
{
	GENERATED_BODY()

public:
		UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray<bool> is_close_door;

		UPROPERTY(EditAnywhere, BlueprintReadWrite)
			TArray<bool> get_close_door;

		UPROPERTY(EditAnywhere, BlueprintReadWrite)
			TArray<bool> is_close_closet_left;

		UPROPERTY(EditAnywhere, BlueprintReadWrite)
			TArray<bool> is_close_closet_right;

		UPROPERTY(EditAnywhere, BlueprintReadWrite)
			TArray<bool> is_close_drawer;

		UPROPERTY(EditAnywhere, BlueprintReadWrite)
			TArray<bool> is_close_refriger;

		UPROPERTY(EditAnywhere, BlueprintReadWrite)
			TArray<bool> is_close_dryer;

		UPROPERTY(EditAnywhere, BlueprintReadWrite)
			TArray<bool> get_close_closet_left;

		UPROPERTY(EditAnywhere, BlueprintReadWrite)
			TArray<bool> get_close_closet_right;

		UPROPERTY(EditAnywhere, BlueprintReadWrite)
			TArray<bool> get_close_drawer;

		UPROPERTY(EditAnywhere, BlueprintReadWrite)
			TArray<bool> get_close_refriger;

		UPROPERTY(EditAnywhere, BlueprintReadWrite)
			TArray<bool> get_close_dryer;

		UPROPERTY(EditAnywhere, BlueprintReadWrite)
			TArray<bool> get_escapekey;

		UPROPERTY(EditAnywhere, BlueprintReadWrite)
			TArray<bool> get_objectkey;

		UPROPERTY(EditAnywhere, BlueprintReadWrite)
			bool get_robot;

		UPROPERTY(EditAnywhere, BlueprintReadWrite)
			int player_id;

		UPROPERTY(EditAnywhere, BlueprintReadWrite)
			bool get_baby;

		UPROPERTY(EditAnywhere, BlueprintReadWrite)
			TArray<bool> set_visible_escapekey;

		UPROPERTY(EditAnywhere, BlueprintReadWrite)
			TArray<bool> set_visible_objectkey;

		UPROPERTY(EditAnywhere, BlueprintReadWrite)
			bool set_visible_robot;

		UPROPERTY(EditAnywhere, BlueprintReadWrite)
			bool set_visible_baby;

		UPROPERTY(EditAnywhere, BlueprintReadWrite)
			bool level_push;

		UPROPERTY(EditAnywhere, BlueprintReadWrite)
			bool otherlevel_push;

		UPROPERTY(EditAnywhere, BlueprintReadWrite)
			bool lvbox_push;

		UPROPERTY(EditAnywhere, BlueprintReadWrite)
			bool lvai_push;

		UPROPERTY(EditAnywhere, BlueprintReadWrite)
			bool key_push;

		UPROPERTY(EditAnywhere, BlueprintReadWrite)
			bool is_close_keypad;

		UPROPERTY(EditAnywhere, BlueprintReadWrite)
			bool get_close_keypad;

		UPROPERTY(EditAnywhere, BlueprintReadWrite)
			bool is_close_keycard;

		UPROPERTY(EditAnywhere, BlueprintReadWrite)
			bool get_close_keycard;

		UPROPERTY(EditAnywhere, BlueprintReadWrite)
			bool is_close_lockbox;

		UPROPERTY(EditAnywhere, BlueprintReadWrite)
			bool get_close_lockbox;

		UPROPERTY(EditAnywhere, BlueprintReadWrite)
			bool is_open_escape;

		UPROPERTY(EditAnywhere, BlueprintReadWrite)
			bool get_open_escape;

		UPROPERTY(EditAnywhere, BlueprintReadWrite)
			bool is_open_coffin;

		UPROPERTY(EditAnywhere, BlueprintReadWrite)
			bool get_open_coffin;

		UPROPERTY(EditAnywhere, BlueprintReadWrite)
			bool is_open_board;

		UPROPERTY(EditAnywhere, BlueprintReadWrite)
			bool get_open_board;

		UPROPERTY(EditAnywhere, BlueprintReadWrite)
			bool is_open_safedoor;

		UPROPERTY(EditAnywhere, BlueprintReadWrite)
			bool get_open_safedoor;

		UPROPERTY(EditAnywhere, BlueprintReadWrite)
			bool obj_robot_open;

		UFUNCTION(BlueprintCallable)
		void initIsopendoor();

		UFUNCTION(BlueprintCallable)
			void initkey();

		UFUNCTION(BlueprintCallable)
			void setIsopendoor();

		UFUNCTION(BlueprintCallable)
			void c2s_Isopendoor();

		UFUNCTION(BlueprintCallable)
			void c2s_key_visible();

		UFUNCTION(BlueprintCallable)
			void c2s_otherobject();

		UFUNCTION(BlueprintCallable)
			void c2s_boxmove();

		UFUNCTION(BlueprintCallable)
			void c2s_aimove();

		UFUNCTION(BlueprintCallable)
			void setIsopencloset();

		UFUNCTION(BlueprintCallable)
			void setIsopendrawer();

		UFUNCTION(BlueprintCallable)
			void setIsopenrefriger();

		UFUNCTION(BlueprintCallable)
			void setIsopendryer();

		UFUNCTION(BlueprintCallable)
			void setIsopenotherobject();

		UFUNCTION(BlueprintCallable)
			void set_movebox();

		UFUNCTION(BlueprintCallable)
			void set_moveai();

		UFUNCTION(BlueprintCallable)
			void set_escapekey();

		UFUNCTION(BlueprintCallable)
			void set_objectkey();

		UFUNCTION(BlueprintCallable)
			void set_levelpush();

		UFUNCTION(BlueprintCallable)
			void set_levelkey();

		UFUNCTION(BlueprintCallable)
			void set_otherlevelpush();

		UFUNCTION(BlueprintCallable)
			void set_boxpush();

		UFUNCTION(BlueprintCallable)
			void set_aipush();

		UPROPERTY(EditAnywhere, BlueprintReadWrite)
			float lv1f_x;
		UPROPERTY(EditAnywhere, BlueprintReadWrite)
			float lv1f_y;
		UPROPERTY(EditAnywhere, BlueprintReadWrite)
			float lv1f_z;

		UPROPERTY(EditAnywhere, BlueprintReadWrite)
			float lvbf_x;
		UPROPERTY(EditAnywhere, BlueprintReadWrite)
			float lvbf_y;
		UPROPERTY(EditAnywhere, BlueprintReadWrite)
			float lvbf_z;

		UPROPERTY(EditAnywhere, BlueprintReadWrite)
			float lv2f_x;
		UPROPERTY(EditAnywhere, BlueprintReadWrite)
			float lv2f_y;
		UPROPERTY(EditAnywhere, BlueprintReadWrite)
			float lv2f_z;

		UPROPERTY(EditAnywhere, BlueprintReadWrite)
			float lvbox_x;
		UPROPERTY(EditAnywhere, BlueprintReadWrite)
			float lvbox_y;
		UPROPERTY(EditAnywhere, BlueprintReadWrite)
			float lvbox_z;

		UPROPERTY(EditAnywhere, BlueprintReadWrite)
			TArray<float> lvmug_x;
		UPROPERTY(EditAnywhere, BlueprintReadWrite)
			TArray<float> lvmug_y;
		UPROPERTY(EditAnywhere, BlueprintReadWrite)
			TArray<float> lvmug_z;

		UPROPERTY(EditAnywhere, BlueprintReadWrite)
			TArray<float> lvmug_rot_x;
		UPROPERTY(EditAnywhere, BlueprintReadWrite)
			TArray<float> lvmug_rot_y;
		UPROPERTY(EditAnywhere, BlueprintReadWrite)
			TArray<float> lvmug_rot_z;

		UPROPERTY(EditAnywhere, BlueprintReadWrite)
			TArray<float> lvmug_vx;
		UPROPERTY(EditAnywhere, BlueprintReadWrite)
			TArray<float> lvmug_vy;
		UPROPERTY(EditAnywhere, BlueprintReadWrite)
			TArray<float> lvmug_vz;

		UPROPERTY(EditAnywhere, BlueprintReadWrite)
			float getlvbox_x;
		UPROPERTY(EditAnywhere, BlueprintReadWrite)
			float getlvbox_y;
		UPROPERTY(EditAnywhere, BlueprintReadWrite)
			float getlvbox_z;

		UPROPERTY(EditAnywhere, BlueprintReadWrite)
			TArray<float> getlvmug_x;
		UPROPERTY(EditAnywhere, BlueprintReadWrite)
			TArray<float> getlvmug_y;
		UPROPERTY(EditAnywhere, BlueprintReadWrite)
			TArray<float> getlvmug_z;

		UPROPERTY(EditAnywhere, BlueprintReadWrite)
			TArray<float> getlvmug_yaw;
		UPROPERTY(EditAnywhere, BlueprintReadWrite)
			TArray<float> getlvmug_pitch;
		UPROPERTY(EditAnywhere, BlueprintReadWrite)
			TArray<float> getlvmug_roll;

		UPROPERTY(EditAnywhere, BlueprintReadWrite)
			TArray<float> getlvmug_vx;
		UPROPERTY(EditAnywhere, BlueprintReadWrite)
			TArray<float> getlvmug_vy;
		UPROPERTY(EditAnywhere, BlueprintReadWrite)
			TArray<float> getlvmug_vz;

		UPROPERTY(EditAnywhere, BlueprintReadWrite)
			bool key_setok;
	
};
