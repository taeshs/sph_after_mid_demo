// Fill out your copyright notice in the Description page of Project Settings.


#include "MyActors.h"

void AMyActors::UpdateNewPlayer(int id)
{


	// 새로운 플레이어를 필드에 스폰
	spawneed.RemoveAt(id);
	spawneed.Insert(true, id);
	logined[id] = false;
	SessionId = ided;

}

void AMyActors::UpdateNewMonster(int id)
{
	

	// 새로운 몬스터를 필드에 스폰
	monsterspawneed.RemoveAt(id);
	monsterspawneed.Insert(true, id);

	monster[id] = false;

	monsterx.RemoveAt(id);
	monstery.RemoveAt(id);
	monsterz.RemoveAt(id);

	monsterx.Insert(monster_x[id], id);
	monstery.Insert(monster_y[id], id);
	monsterz.Insert(monster_z[id], id);
	
	SessionId = id;

	
}

void  AMyActors::actormove()
{
	moveId = moveided;

		actorx.RemoveAt(moveId);
		actory.RemoveAt(moveId);
		actorz.RemoveAt(moveId);

		actoryaw.RemoveAt(moveId);
		actorpitch.RemoveAt(moveId);
		actorroll.RemoveAt(moveId);

		actorvx.RemoveAt(moveId);
		actorvy.RemoveAt(moveId);
		actorvz.RemoveAt(moveId);

		actorflash.RemoveAt(moveId);

	actorx.Insert(cactorx[moveId], moveId);
	actory.Insert(cactory[moveId], moveId);
	actorz.Insert(cactorz[moveId], moveId);

	actoryaw.Insert(cactoryaw[moveId], moveId);
	actorpitch.Insert(cactorpit[moveId], moveId);
	actorroll.Insert(cactorroll[moveId], moveId);

	actorvx.Insert(cactorvx[moveId], moveId);
	actorvy.Insert(cactorvy[moveId], moveId);
	actorvz.Insert(cactorvz[moveId], moveId);
	actorflash.Insert(cflash[moveId], moveId);

}

void  AMyActors::Npcmove()
{
	npcmoveId = cmove_npcid;

	monsterx.RemoveAt(npcmoveId);
	monstery.RemoveAt(npcmoveId);
	monsterz.RemoveAt(npcmoveId);

	monsterx.Insert(monster_x[npcmoveId], npcmoveId);
	monstery.Insert(monster_y[npcmoveId], npcmoveId);
	monsterz.Insert(monster_z[npcmoveId], npcmoveId);

	npctoplayerId = npc_p_id;

	//UE_LOG(LogClass, Log, TEXT("monster set %d"), npctoplayerId);
}

void  AMyActors::actordead()
{
	
	isactordestroy = true;
	deadId = deadided;
	
}
// Sets default values
AMyActors::AMyActors()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AMyActors::BeginPlay()
{
	Super::BeginPlay();


	isactordestroy = false;


	actorx.Init(-1620.0f, 4);
	actory.Init(-29.0f, 4);
	actorz.Init(117.0f, 4);
	actoryaw.Init(0.0f, 4);
	actorpitch.Init(0.0f, 4);
	actorroll.Init(0.0f, 4);
	actorvx.Init(0.0f, 4);
	actorvy.Init(0.0f, 4);
	actorvz.Init(0.0f, 4);
	actorflash.Init(false, 4);

	monsterx.Init(0.0f, 4);
	monstery.Init(0.0f, 4);
	monsterz.Init(0.0f, 4);

	spawneed.Init(false, 4);
	monsterspawneed.Init(false, 4);

	

	
}

// Called every frame
void AMyActors::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);


	for (int i=0;i<4;++i)
	{
		if (logined[i])
			UpdateNewPlayer(i);

		if (monster[i])
			UpdateNewMonster(i);
	}
	

	if (is_destroy)
	{
		actordead();
		is_destroy = false;
		
	}

	if (is_move)
		actormove();

	if(npc_is_move)
		Npcmove();

}

