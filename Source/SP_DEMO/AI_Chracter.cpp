// Fill out your copyright notice in the Description page of Project Settings.


#include "AI_Chracter.h"

// Sets default values
AAI_Chracter::AAI_Chracter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	AI_move = false;

}

// Called when the game starts or when spawned
void AAI_Chracter::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AAI_Chracter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (AI_move)
	{
		cAI_move = true;
	}

}

// Called to bind functionality to input
void AAI_Chracter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

