﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "ClientGameModeBase.h"
#include <thread>
#include <string>
#include <unordered_map>
#include "MyActors.h"
#include "UObject/ConstructorHelpers.h"
#include "Kismet/GameplayStatics.h"
#include "GameFramework/Character.h"
#include "GameFramework/Actor.h"
#include "IPContainer.h"

unsigned short remain_packet_size;

SOCKET hSock = INVALID_SOCKET;
char recv_buf[MAX_BUFFER];
char recv_packet_buf[MAX_PACKET_SIZE1];
int					SessionId;
int cur_player;
bool bIsgameStart = false;


//CharacterPacket Newplayer;
//CharacterPacket playinfo; //���� �÷��̾�
AClientGameModeBase* base;
thread* recv_t;
//unordered_map <int, CharacterPacket> players;


AClientGameModeBase::AClientGameModeBase()
{
	UE_LOG(LogClass, Warning, TEXT("-------------init, connect here"));

	PrimaryActorTick.bStartWithTickEnabled = true;
	PrimaryActorTick.bCanEverTick = true;

	cescape_key = 0;
	cnumof_key = 0;

	info_ok = false;

	Message = TEXT("한글초기화");

	/*
	static ConstructorHelpers::FObjectFinder<UBlueprint> Spawnchar(TEXT("/Game/ThirdPersonBP/Blueprints/test"));
	if (Spawnchar.Succeeded() && Spawnchar.Object) {
		UE_LOG(LogTemp, Warning, TEXT("-------------Spawnchar.Succeeded------------"));
		charBP = Spawnchar.Object;
	}

	//AActor* spawnchar = GetWorld()->SpawnActor(charBP->GeneratedClass);
	//spawnchar->SetActorLocation(FVector(-1620.0f, -29.0f, 117.0f));
	
	*/



}

AClientGameModeBase::~AClientGameModeBase()
{
}




void AClientGameModeBase::send_login_packet(SOCKET socket)
{
	c2s_packet_login packet;
	packet.size = sizeof(packet);
	packet.type = C2S_PACKET_LOGIN;
	strcpy_s(packet.name, to_string(rand() % 100).c_str());


	send(socket, reinterpret_cast<char*>(&packet), packet.size, 0);
	
}


void AClientGameModeBase::send_ready_packet()
{
	c2s_packet_ready packet;
	packet.size = sizeof(packet);
	packet.type = C2S_PACKET_READY;
	packet.ready = true;


	send(hSock, reinterpret_cast<char*>(&packet), packet.size, 0);

}

void AClientGameModeBase::send_start_packet()
{
	c2s_packet_start packet;
	packet.size = sizeof(packet);
	packet.type = C2S_PACKET_START_CLICK;
	packet.start = true;


	send(hSock, reinterpret_cast<char*>(&packet), packet.size, 0);

}

void AClientGameModeBase::send_playerinfo()
{
	c2s_packet_playerinfo packet;
	packet.size = sizeof(packet);
	packet.type = C2S_PACKET_PLAYER_INFO;


	send(hSock, reinterpret_cast<char*>(&packet), packet.size, 0);

}

void AClientGameModeBase::send_key_packet()
{
	c2s_packet_key packet;
	packet.size = sizeof(packet);
	packet.type = C2S_PACKET_KEY;
	packet.escape_key = cescape_key;
	packet.num_key = cnumof_key;


	send(hSock, reinterpret_cast<char*>(&packet), packet.size, 0);

}

void AClientGameModeBase::send_key_visible_packet()
{
	c2s_packet_key_visible packet;
	packet.size = sizeof(packet);
	packet.type = C2S_PACKET_KEY_VISIBLE;

	for (int i = 0; i < 3; ++i)
	{
		packet.get_escapekey[i] = cget_escapekey[i];
	}

	for (int i = 0; i < 11; ++i)
	{
		packet.get_objectkey[i] = cget_objectkey[i];
	}

	send(hSock, reinterpret_cast<char*>(&packet), packet.size, 0);

}

void AClientGameModeBase::send_otherobject_packet()
{
	c2s_packet_otherobject packet;
	packet.size = sizeof(packet);
	packet.type = C2S_PACKET_OTHEROBJECT;

	packet.is_open_coffin = cis_open_coffin;
	packet.get_robot = cget_robot;
	packet.get_baby = cget_baby;
	packet.is_open_board = cis_open_board;
	packet.is_open_safedoor = cis_open_safedoor;

	send(hSock, reinterpret_cast<char*>(&packet), packet.size, 0);

}

void AClientGameModeBase::send_boxmove_packet()
{
	c2s_packet_boxmove packet;
	packet.size = sizeof(packet);
	packet.type = C2S_PACKET_BOXMOVE;

	packet.box_x = box_x;
	packet.box_y = box_y;
	packet.box_z = box_z;


	send(hSock, reinterpret_cast<char*>(&packet), packet.size, 0);

}

void AClientGameModeBase::send_aimove_packet()
{
	c2s_packet_aimove packet;
	packet.size = sizeof(packet);
	packet.type = C2S_PACKET_AIMOVE;

	for (int i = 0; i < 2; ++i)
	{
		packet.mug_x[i] = mug_x[i];
		packet.mug_y[i] = mug_y[i];
		packet.mug_z[i] = mug_z[i];

		packet.mug_roll[i] = mug_rot_x[i];
		packet.mug_pitch[i] = mug_rot_y[i];
		packet.mug_yaw[i] = mug_rot_z[i];

		packet.mug_vx[i] = mug_vx[i];
		packet.mug_vy[i] = mug_vy[i];
		packet.mug_vz[i] = mug_vz[i];
	}




	send(hSock, reinterpret_cast<char*>(&packet), packet.size, 0);

}

void AClientGameModeBase::send_packet()
{

	if (testnode == true)
	{

		string str(testtext);

		Message = *FString(str.c_str());
		testnode = false;
		chat_show = true;
	}
	if(l2cb_push == true)
	{ 
	send_object_packet();

	l2cb_push = false;
	}

	if (otherobject_push)
	{
		send_otherobject_packet();

		otherobject_push = false;
	}

	if (box_push)
	{
		send_boxmove_packet();
		box_push = false;
	}

	
	if (ai_push)
	{
		send_aimove_packet();
		ai_push = false;
	}
	

	if (ckey_push == true)
	{
		send_key_visible_packet();

		ckey_push = false;
	}

	if (send_chatting == true)
	{

		send_chatting = false;

		std::string strmsg = TCHAR_TO_ANSI(*Message);

		wstring wstrmsg;

		wstrmsg.assign(strmsg.begin(), strmsg.end());

		//wstring wstrmsg = strmsg;

		const wchar_t* wchat = wstrmsg.c_str();

		send_chat_packet(wchat);
	}
}

void AClientGameModeBase::send_chat_packet(const wchar_t chat[])
{

	//채팅 전송
	c2s_packet_chat packet;
	packet.type = C2S_PACKET_CHAT;
	packet.size = sizeof(packet);
	wcscpy_s(packet.message, chat);
	//strcpy(packet.message, chat);
	//cout << packet.message << endl;
	size_t sent = 0;

	send(hSock, reinterpret_cast<char*>(&packet), packet.size, 0);
}

void AClientGameModeBase::Settext(string text)
{
	string test = text;

	char ch[50];

	strcpy(ch, test.c_str());

	strcpy(testtext, ch);
	
}


void AClientGameModeBase::send_move_packet()
{
	c2s_packet_move packet;
	packet.size = sizeof(packet);
	packet.type = C2S_PACKET_MOVE;

	auto Player = Cast<ACharacter>(UGameplayStatics::GetPlayerCharacter(this, 0));

	// �÷��̾��� ��ġ�� ������	
	const auto& Location = Player->GetActorLocation();
	const auto& Rotation = Player->GetActorRotation();
	const auto& Velocity = Player->GetVelocity();
	

	packet.x = Location.X;
	packet.y = Location.Y;
	packet.z = Location.Z;

	packet.yaw = Rotation.Yaw;
	packet.pitch = Rotation.Pitch;
	packet.roll = Rotation.Roll;

	packet.vx = Velocity.X;
	packet.vy = Velocity.Y;
	packet.vz = Velocity.Z;

	packet.flashlight = get_flashlight;

	//UE_LOG(LogClass, Log, TEXT("flash : %s"), get_flashlight);

	send(hSock, reinterpret_cast<char*>(&packet), packet.size, 0);
}

void AClientGameModeBase::send_object_packet()
{
	c2s_packet_object packet;
	packet.size = sizeof(packet);
	packet.type = C2S_PACKET_OBJECT;


	for (int i = 0; i < 21; ++i)
	{
		packet.is_close_door[i] = cis_close_door[i];
	}

	for (int i = 0; i < 6; ++i)
	{
		packet.is_close_closet_left[i] = cis_close_closet_left[i];
		packet.is_close_closet_right[i] = cis_close_closet_right[i];
	}

	for (int i = 0; i < 8; ++i)
	{
		packet.is_close_drawer[i] = cis_close_drawer[i];
	}

	for (int i = 0; i < 4; ++i)
	{
		packet.is_close_refriger[i] = cis_close_refriger[i];
	}

	for (int i = 0; i < 2; ++i)
	{
		packet.is_close_dryer[i] = cis_close_dryer[i];
	}


	packet.is_close_lockbox = cis_close_lockbox;
	packet.is_close_keypad = cis_close_keypad;
	packet.is_close_keycard = cis_close_keycard;
	packet.is_open_escape = cis_open_escape;


	send(hSock, reinterpret_cast<char*>(&packet), packet.size, 0);
}

void AClientGameModeBase::Is_IpSet()
{

	if (is_setip == true)
	{
		is_setip = false;

		SetIpAddress();

	}

}

void AClientGameModeBase::Setlobby()
{


	if (clobby == true)
	{
		clobby = false;
		lobby_ok = true;

		Playerid = SessionId;
		cplayer_id = SessionId; //clientsocket

		//UE_LOG(LogClass, Log, TEXT("lobby test"));

		for (int i = 0; i < cur_player; ++i)
		{
			UE_LOG(LogClass, Log, TEXT("lobby test"));
			if(is_connect.IsValidIndex(i) == true)
			{ 
				host.RemoveAt(i);
				is_connect.RemoveAt(i);
			}

				host.Insert(chost[i], i);
				is_connect.Insert(cis_connect[i], i);
		
			
		}

	}
	

}

void AClientGameModeBase::SetStatus()
{


	if (cstatus == true)
	{
		cstatus = false;
		status_ok = true;


		UE_LOG(LogClass, Log, TEXT("status test"));

		for (int i = 0; i < cur_player; ++i)
		{
			//UE_LOG(LogClass, Log, TEXT("lobby test"));
			if (ready.IsValidIndex(i) == true)
			{
				ready.RemoveAt(i);
				
			}

			ready.Insert(cready[i], i);


		}

	}


}

void AClientGameModeBase::SetStart()
{

	start_ok = bIsgameStart;

}

void AClientGameModeBase::SetKeynum()
{


	if (ckey == true)
	{
		ckey = false;
		Key_ok = true;

		cescape_key = s2c_ekey;
		cnumof_key = s2c_obkey;

	}


}

void AClientGameModeBase::PlayerInfo()
{
	if (bIsgameStart == true)
	{
		info_ok = true;
	}

}

void AClientGameModeBase::SetSLobbyout()
{
	destroy_ok = is_destroy;

	is_destroy = false;

	if(destroy_ok == true)
	{ 
	for (int i = 0; i < cur_player; ++i)
	{
		//UE_LOG(LogClass, Log, TEXT("lobby test"));
		if (is_connect.IsValidIndex(i) == true)
		{
			is_connect.RemoveAt(i);
		}

		is_connect.Insert(cis_connect[i], i);


	}
	cur_player--;
	}


}



void AClientGameModeBase::MoveProcess()
{
	if (bIsgameStart)
	{
		movetest = true;

		is_move = false;

		bNewPlayerEntered = false;

		get_flashlight = true;

		send_chatting = false;
		chat_show = false;

		for (int i = 0; i < 21; ++i)
		{
			cis_close_door[i] = true;
		}

		for (int i = 0; i < 21; ++i)
		{
			getis_close_door[i] = true;
		}

		GetWorldTimerManager().SetTimer(SendMoveHandle, this, &AClientGameModeBase::send_move_packet, 0.016f, true);
		//GetWorldTimerManager().SetTimer(SendMonsterMoveHandle, this, &AClientGameModeBase::send_aimove_packet, 0.016f, true);
	}
}


void AClientGameModeBase::ProcessPacket()
{
	static bool first_time = true;
	byte msg_type = recv_packet_buf[sizeof(byte)];

	
	switch (msg_type)
	{
	case S2C_PACKET_LOBBY:
	{
		s2c_packet_lobby* packet = reinterpret_cast<s2c_packet_lobby*>(recv_packet_buf);

		cur_player++;

		int p_id = packet->id;

		chost[p_id] = packet->host;
		cis_connect[p_id] = true;

		clobby = true;
		

		//Setlobby();
		
		//host.Insert(packet->host,p_id);
		//is_connect.Insert(true, p_id);


		//player[p_id].is_connect = true;
		//player[p_id].host = packet->host;

	}
	break;
	case S2C_PACKET_READY:
	{
		s2c_packet_ready* packet = reinterpret_cast<s2c_packet_ready*>(recv_packet_buf);

		int p_id = packet->id;

		cready[p_id] = packet->ready;

		cstatus = true;
		//chost[p_id] = packet->host;
		//cis_connect[p_id] = true;

		//clobby = true;


	}
	break;
	case S2C_PACKET_GAME_START:
	{
		s2c_packet_start* packet = reinterpret_cast<s2c_packet_start*>(recv_packet_buf);

		bIsgameStart = packet->start;


	}
	break;
	case S2C_PACKET_LOGIN_INFO:
	{
		s2c_packet_login_info* packet = reinterpret_cast<s2c_packet_login_info*>(recv_packet_buf);
		SessionId = packet->id;


	
	}
	break;
	case S2C_PACKET_KEY_INFO:
	{
		s2c_packet_key_info* packet = reinterpret_cast<s2c_packet_key_info*>(recv_packet_buf);
		
		firstf_x = packet->firstf_x;
		firstf_y = packet->firstf_y;
		firstf_z = packet->firstf_z;

		bf_x = packet->bf_x;
		bf_y = packet->bf_y;
		bf_z = packet->bf_z;

		secf_x = packet->secf_x;
		secf_y = packet->secf_y;
		secf_z = packet->secf_z;

		key_init = true;

	}
	break;
	case S2C_PACKET_PC_LOGIN: //Ÿ �÷��̾� �α���
	{
		s2c_packet_pc_login* my_packet = reinterpret_cast<s2c_packet_pc_login*>(recv_packet_buf);
		int id = my_packet->id;

		if (id <= MAX_USER) {
			logined[id] = true;
			ided = id; //2��°�� 1
			
		}
		else
		{
			
			int monsterid = id - 5;
			monster[monsterid] = true;
			monsterided = monsterid;

			monster_x[monsterid] = my_packet->x;
			monster_y[monsterid] = my_packet->y;
			monster_z[monsterid] = my_packet->z;
			
				/*
			monster[id] = true;
			monsterided = id;

			monster_x[id] = my_packet->x;
			monster_y[id] = my_packet->y;
			monster_z[id] = my_packet->z;

			*/
			
		}
		
		break;
	}
	case S2C_PACKET_PC_MOVE:
	{
		s2c_packet_pc_move* my_packet = reinterpret_cast<s2c_packet_pc_move*>(recv_packet_buf);
		int other_id = my_packet->id;
		if (other_id == SessionId) {


		}
		else if (other_id < MAX_USER) {
			moveided = other_id;

			cactorx[other_id] = my_packet->x;
			cactory[other_id] = my_packet->y;
			cactorz[other_id] = my_packet->z;

			cactoryaw[other_id] = my_packet->yaw;
			cactorpit[other_id] = my_packet->pitch;
			cactorroll[other_id] = my_packet->roll;

			cactorvx[other_id] = my_packet->vx;
			cactorvy[other_id] = my_packet->vy;
			cactorvz[other_id] = my_packet->vz;

			cflash[other_id] = my_packet->flashlight;

			is_move = true;

		}

		break;
	}
	case S2C_PACKET_NPC_MOVE:
	{
		s2c_packet_npc_move* my_packet = reinterpret_cast<s2c_packet_npc_move*>(recv_packet_buf);
		int other_id = my_packet->id -5;

		cmove_npcid = other_id;

		monster_x[other_id] = my_packet->x;
		monster_y[other_id] = my_packet->y;
		monster_z[other_id] = my_packet->z;

		if (SessionId == my_packet->player_id)
		{
			npc_p_id = 99;
		}
		else
			npc_p_id = my_packet->player_id;

		npc_is_move = true;

		break;
	}
	case S2C_PACKET_OBJECT:
	{
		s2c_packet_object* my_packet = reinterpret_cast<s2c_packet_object*>(recv_packet_buf);
		int other_id = my_packet->id;

			for (int i = 0; i < 21; ++i)
			{
				getis_close_door[i] = my_packet->is_close_door[i];
			}

			for (int i = 0; i < 6; ++i)
			{
				getis_close_closet_left[i] = my_packet->is_close_closet_left[i];
				getis_close_closet_right[i] = my_packet->is_close_closet_right[i];
			}

			for (int i = 0; i < 8; ++i)
			{
				getis_close_drawer[i] = my_packet->is_close_drawer[i];
			}

			for (int i = 0; i < 4; ++i)
			{
				getis_close_refriger[i] = my_packet->is_close_refriger[i];
			}

			for (int i = 0; i < 4; ++i)
			{
				getis_close_dryer[i] = my_packet->is_close_dryer[i];
			}

			getis_close_lockbox = my_packet->is_close_lockbox;

			cget_close_keypad = my_packet->is_close_keypad;
			cget_close_keycard = my_packet->is_close_keycard;
			cget_open_escape = my_packet->is_open_escape;

			cs2c_door_push = true;

		break;

	}

	case S2C_PACKET_OTHEROBJECT:
	{
		s2c_packet_otherobject* my_packet = reinterpret_cast<s2c_packet_otherobject*>(recv_packet_buf);

		cget_open_coffin = my_packet->is_open_coffin;
		cset_visible_robot = my_packet->get_robot;
		cset_visible_baby = my_packet->get_baby;
		cget_open_board = my_packet->is_open_board;
		cget_open_safedoor = my_packet->is_open_safedoor;

		cs2c_otherobject_push = true;

		break;

	}
	case S2C_PACKET_BOXMOVE:
	{
		s2c_packet_boxmove* my_packet = reinterpret_cast<s2c_packet_boxmove*>(recv_packet_buf);

		getbox_x = my_packet->box_x;
		getbox_y = my_packet->box_y;
		getbox_z = my_packet->box_z;


		cs2c_box_push = true;

		break;

	}
	case S2C_PACKET_AIMOVE:
	{
		s2c_packet_aimove* my_packet = reinterpret_cast<s2c_packet_aimove*>(recv_packet_buf);

		for (int i = 0; i < 2; ++i)
		{
			getmug_x[i] = my_packet->mug_x[i];
			getmug_y[i] = my_packet->mug_y[i];
			getmug_z[i] = my_packet->mug_z[i];

			getmug_yaw[i] = my_packet->mug_yaw[i];
			getmug_pitch[i] = my_packet->mug_pitch[i];
			getmug_roll[i] = my_packet->mug_roll[i];

			getmug_vx[i] = my_packet->mug_vx[i];
			getmug_vy[i] = my_packet->mug_vy[i];
			getmug_vz[i] = my_packet->mug_vz[i];
		}


		cs2c_ai_push = true;

		break;

	}
	case S2C_PACKET_CHAT:
	{
		s2c_packet_chat* my_packet = reinterpret_cast<s2c_packet_chat*>(recv_packet_buf);
		int o_id = my_packet->id;

		wstring wmassage = my_packet->message;
		string message;

		message.assign(wmassage.begin(), wmassage.end());


		string temp = "[Player: ";
		temp += to_string(o_id);
		temp += "] => ";
		temp += message;

		Settext(temp);
		
		//FString UE4Str0(temp.c_str());
		//Message = UE4Str;


		//UE_LOG(LogTemp, Log, TEXT("Character Name :: %s"), *Message);

		testnode = true;
		

		break;
	}
	case S2C_PACKET_KEY:
	{
		s2c_packet_key* my_packet = reinterpret_cast<s2c_packet_key*>(recv_packet_buf);
		int other_id = my_packet->id;

		s2c_ekey = my_packet->escape_key;
		s2c_obkey = my_packet->num_key;

		ckey = true;

		break;

	}
	case S2C_PACKET_KEY_VISIBLE:
	{
		s2c_packet_key_visible* my_packet = reinterpret_cast<s2c_packet_key_visible*>(recv_packet_buf);


		for (int i = 0; i < 3; ++i)
		{
			s2c_get_escapekey[i] = my_packet->get_escapekey[i];
		}

		for (int i = 0; i < 11; ++i)
		{
			s2c_get_objectkey[i] = my_packet->get_objectkey[i];
		}

		cs2c_key_visible = true;

		break;

	}
	case S2C_PACKET_PC_LOGOUT:
	{
		s2c_packet_pc_logout* my_packet = reinterpret_cast<s2c_packet_pc_logout*>(recv_packet_buf);
		int other_id = my_packet->id;
		if (other_id == SessionId) {

			//�÷��̾� �����
		}
		else if (other_id < MAX_USER) {
			is_destroy = true;
			deadided = other_id;

			//lobby
			cis_connect[other_id] = false;

			//�ٸ� �÷��̾� �α׾ƿ�� �ٸ� �÷��̾� �����
		}
		break;
	}
	default:
		UE_LOG(LogClass, Log, TEXT("Unknown PACKET type"));
	}

	
}



void RecvThread()
{

		while (true)
		{

			int len = recv(hSock, recv_buf, MAX_BUFFER, 0);

			// ��Ŷ ���� + ��Ŷó��
			if (len == 0) {

				return;
			}
			if (len < 0)
			{
				continue;
				
			}

			// packet ����

			int rest_packet_size = len;
			int cur_recv_buf_idx = 0;

			while (rest_packet_size) {
				int required;
				if (remain_packet_size == 0) required = recv_buf[cur_recv_buf_idx];
				else required = recv_packet_buf[0] - remain_packet_size;
				if (required > rest_packet_size) {
					memcpy(recv_packet_buf + remain_packet_size, recv_buf + cur_recv_buf_idx, rest_packet_size);
					remain_packet_size += rest_packet_size;
					break;
				}


				memcpy(recv_packet_buf + remain_packet_size, recv_buf + cur_recv_buf_idx, required);
				cur_recv_buf_idx += required;
				rest_packet_size -= required;
				remain_packet_size = 0;
				
				base->ProcessPacket();
			}

		}
	
}

void AClientGameModeBase::test()
{
	tested = true;
}


void AClientGameModeBase::SetIpAddress1(int a, int b, int c, int d)
{
	
	char* temp[20];
	
	string ipchange1 = to_string(a);
	const char* ip1 = ipchange1.c_str();

	strcpy((char*)temp, ip1);
	strcat((char*)temp, ".");

	string ipchange2 = to_string(b);
	const char* ip2 = ipchange2.c_str();

	strcat((char*)temp, ip2);
	strcat((char*)temp, ".");

	string ipchange3 = to_string(c);
	const char* ip3 = ipchange3.c_str();

	strcat((char*)temp, ip3);
	strcat((char*)temp, ".");

	string ipchange4 = to_string(d);
	const char* ip4 = ipchange4.c_str();

	strcat((char*)temp, ip4);


	UE_LOG(LogClass, Log, TEXT("Character Name :: %s"), temp);

	IpAddress = (char*)temp;
}

void AClientGameModeBase::SetIpAddress()
{
	Fint4 ContIp = GetContainedIp();

	SetIpAddress1(ContIp.a, ContIp.b, ContIp.c, ContIp.d);

	ipsetting_ok = false;
	lobby_ok = false;
	status_ok = false;
	/*
	*초기화 추후에 옮길것
	movetest = true;

	is_move = false;

	bNewPlayerEntered = false;

	get_flashlight = true;

	send_chatting = false;
	chat_show = false;

	for (int i = 0; i < 22; ++i)
	{
		cis_close_door[i] = true;
	}

	for (int i = 0; i < 22; ++i)
	{
		getis_close_door[i] = true;
	}
	*/

	bIsConnected = Init(hSock);

	if (bIsConnected)
	{
		UE_LOG(LogClass, Log, TEXT("IOCP Server connect success!"));

		//host.Init(false, 4);
		//ready.Init(false, 4);
		//start.Init(false, 4);
		//is_connect.Init(false, 4);
		cur_player = 0;

		ipsetting_ok = true;

		send_login_packet(hSock);

		remain_packet_size = 0;

		recv_t = new thread{ RecvThread };

		//GetWorldTimerManager().SetTimer(SendMoveHandle, this, &AClientGameModeBase::send_move_packet, 0.016f, true);
		//timer로 주기적으로 move패킷 전달
	}


	//send_move_packet(hSock);
}

/*void AClientGameModeBase::BeginPlay()
{
	UE_LOG(LogTemp, Warning, TEXT("-------------BeginPlay------------"));

	// ���� ip �޴� �κ����� �ű� ���� ������ ����� ����.
	


}*/

/*
void AClientGameModeBase::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);


}
*/