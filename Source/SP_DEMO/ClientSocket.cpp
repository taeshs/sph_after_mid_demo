#include "ClientSocket.h"
#pragma warning(disable : 4834)
#pragma warning(disable:4996) 

#define _CRT_SECURE_NO_WARNINGS


bool Init(SOCKET& hSock)
{
	WSADATA WSAData;
	int nRet = WSAStartup(MAKEWORD(2, 2), &WSAData);
	if (nRet != 0)
		return false;

	hSock = WSASocket(AF_INET, SOCK_STREAM, 0, NULL, 0, WSA_FLAG_OVERLAPPED);
	if (hSock == INVALID_SOCKET)
		return false;

	UE_LOG(LogClass, Log, TEXT("connecting"));

	SOCKADDR_IN serverAddr;
	memset(&serverAddr, 0, sizeof(SOCKADDR_IN));
	serverAddr.sin_family = AF_INET;
	serverAddr.sin_port = htons(SERVER_PORT);
	//serverAddr.sin_addr.S_un.S_addr = htonl(INADDR_ANY); //htonl(ip_addr);
    //serverAddr.sin_addr.s_addr = inet_addr(SERVER_IP);
	serverAddr.sin_addr.s_addr = inet_addr(IpAddress);

	nRet = WSAConnect(hSock, (SOCKADDR*)&serverAddr, sizeof(serverAddr), NULL, NULL, NULL, NULL);

	
	if (nRet == SOCKET_ERROR)
		return false;

	UE_LOG(LogClass, Log, TEXT("connecting1"));

	return true;

}
