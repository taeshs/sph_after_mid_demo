// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ClientSocket.h"
#include "GameFramework/Actor.h"
#include "Otherobject.generated.h"

UCLASS()
class SP_DEMO_API AOtherobject : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AOtherobject();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool obj_coffin_open;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool obj_whiteboard;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool box_move;


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
