﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "ClientSocket.h"
#include "ClientGameModeBase.generated.h"

/**
 *
 */

UCLASS()
class SP_DEMO_API AClientGameModeBase : public AGameModeBase
{
	GENERATED_BODY()

public:
	AClientGameModeBase();
	~AClientGameModeBase();

	//virtual void Tick(float DeltaSeconds) override;
	//virtual void BeginPlay() override;

	void  ProcessPacket();
	void send_move_packet();
	void send_chat_packet(const wchar_t chat[]);
	void send_object_packet();
	void send_key_visible_packet();
	void send_otherobject_packet();
	void send_boxmove_packet();
	void send_aimove_packet();
	
	void send_login_packet(SOCKET socket);
	void SetIpAddress1(int, int, int, int);
	void Settext(string text);

	UFUNCTION(BlueprintCallable)
	void Setlobby();

	UFUNCTION(BlueprintCallable)
		void SetStatus();

	UFUNCTION(BlueprintCallable)
		void SetStart();

	UFUNCTION(BlueprintCallable)
		void SetSLobbyout();

	UFUNCTION(BlueprintCallable)
		void SetKeynum();

	UFUNCTION(BlueprintCallable)
		void PlayerInfo();

	UFUNCTION(BlueprintCallable)
		void MoveProcess();

	UFUNCTION(BlueprintCallable)
	void SetIpAddress();

	UFUNCTION(BlueprintCallable)
		void send_packet();

	UFUNCTION(BlueprintCallable)
		void send_ready_packet();

	UFUNCTION(BlueprintCallable)
		void send_start_packet();

	UFUNCTION(BlueprintCallable)
		void send_key_packet();

	UFUNCTION(BlueprintCallable)
		void send_playerinfo();


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Spawning")
		bool get_flashlight;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FString Message;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool send_chatting;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool chat_show;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool ipsetting_ok;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool lobby_ok;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool status_ok;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool start_ok;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool destroy_ok;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool info_ok;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool Key_ok;

	UFUNCTION(BlueprintCallable)
		void Is_IpSet();

	void test();

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere, Category = "Spawning")
		UBlueprint* charBP;

	FString UE4Str;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray<bool> host;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray<bool> ready;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray<bool> start;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray<bool> is_connect;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int Playerid;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int cescape_key;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int cnumof_key;

private:
	//bool bIsConnected;
	bool bNewPlayerEntered;
	bool tested;
	bool movetest;
	bool g_flash;
	
	FTimerHandle SendMoveHandle;	// 동기화 타이머 핸들러
	FTimerHandle SendMonsterMoveHandle;	// 동기화 타이머 핸들러
};

