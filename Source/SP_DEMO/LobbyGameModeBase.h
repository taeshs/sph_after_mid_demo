// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ClientSocket.h"
#include "GameFramework/GameModeBase.h"
#include "LobbyGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class SP_DEMO_API ALobbyGameModeBase : public AGameModeBase
{
	GENERATED_BODY()

		/*
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	*/
public:
	UFUNCTION(BlueprintCallable)
		void Is_IpSet();
	
};
