// Fill out your copyright notice in the Description page of Project Settings.


#include "Otherobject.h"

// Sets default values
AOtherobject::AOtherobject()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	obj_coffin_open = false;
	obj_whiteboard = false;
	box_move = false;

}

// Called when the game starts or when spawned
void AOtherobject::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AOtherobject::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (obj_coffin_open)
	{
		cotherobject_open = true;
		obj_coffin_open = false;
	}

	if (obj_whiteboard)
	{
		cotherobject_open = true;
		obj_whiteboard = false;
	}

	if (box_move)
	{
		cbox_move = true;
	}
}

