// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "MyCharacter.generated.h"

UCLASS()
class SP_DEMO_API AMyCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AMyCharacter();
	int		SessionId;		// 플레이어 고유 아이디

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere, Category = "Spawning")
		UBlueprint* charBP;

	UFUNCTION(BluePrintCallable)
	void testchar();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	bool tested;
	bool bIsConnected;
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

};
