// Fill out your copyright notice in the Description page of Project Settings.


#include "DoorBlueprint.h"

// Sets default values
ADoorBlueprint::ADoorBlueprint()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	is_push = false;
	closet_is_push = false;
	drawer_is_push = false;
	refriger_is_push = false;
	dryer_is_push = false;
}

/*
// Called when the game starts or when spawned
void ADoorBlueprint::BeginPlay()
{
	Super::BeginPlay();

	
	
}
*/

// Called every frame
void ADoorBlueprint::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (is_push)
	{
		cis_push = true;
		is_push = false;
	}

	if (closet_is_push)
	{
		cis_closet_push = true;
		closet_is_push = false;
	}

	if (drawer_is_push)
	{
		cis_drawer_push = true;
		drawer_is_push = false;
	}

	if (refriger_is_push)
	{
		cis_refriger_push = true;
		refriger_is_push = false;
	}

	if (dryer_is_push)
	{
		cis_dryer_push = true;
		dryer_is_push = false;
	}
}

