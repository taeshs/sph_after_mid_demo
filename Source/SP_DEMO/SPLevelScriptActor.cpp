// Fill out your copyright notice in the Description page of Project Settings.


#include "SPLevelScriptActor.h"

void ASPLevelScriptActor::initIsopendoor()
{
	is_close_door.Init(true, 21);
	get_close_door.Init(true, 21);

	is_close_closet_left.Init(false, 6);
	get_close_closet_left.Init(false, 6);

	is_close_closet_right.Init(false, 6);
	get_close_closet_right.Init(false, 6);

	is_close_drawer.Init(false, 8);
	get_close_drawer.Init(false, 8);

	is_close_refriger.Init(false, 4);
	get_close_refriger.Init(false, 4);

	is_close_dryer.Init(false, 2);
	get_close_dryer.Init(false, 2);

	get_escapekey.Init(false, 3);
	set_visible_escapekey.Init(false, 3);

	get_objectkey.Init(false, 11);
	set_visible_objectkey.Init(false, 11);

	//lvmug_x.Insert(-1468.0f, 0);
	//lvmug_y.Insert(41.0f, 0);
	//lvmug_z.Insert(-575.0f, 0);

	//lvmug_x.Insert(420.0f, 1);
	//lvmug_y.Insert(850.0f, 1);
	//lvmug_z.Insert(151.21f, 1);

	//lvmug_x.Insert(-2050.0f, 2);
	//lvmug_y.Insert(-2820.0f, 2);
	//lvmug_z.Insert(-575.0f, 2);

	lvmug_x.Init(0.0f, 2);
	lvmug_y.Init(0.0f, 2);
	lvmug_z.Init(0.0f, 2);

	lvmug_rot_x.Init(0.0f, 2);
	lvmug_rot_y.Init(0.0f, 2);
	lvmug_rot_z.Init(0.0f, 2);

	lvmug_vx.Init(0.0f, 2);
	lvmug_vy.Init(0.0f, 2);
	lvmug_vz.Init(0.0f, 2);

	getlvmug_x.Init(0.0f, 2);
	getlvmug_y.Init(0.0f, 2);
	getlvmug_z.Init(0.0f, 2);

	getlvmug_yaw.Init(0.0f, 2);
	getlvmug_pitch.Init(0.0f, 2);
	getlvmug_roll.Init(0.0f, 2);

	getlvmug_vx.Init(0.0f, 2);
	getlvmug_vy.Init(0.0f, 2);
	getlvmug_vz.Init(0.0f, 2);

	//lvbox_x = 1631.0;
	//lvbox_y = 1110.0;
	//lvbox_z = -630.0;

	level_push = false;
	key_push = false;

	is_close_keypad = false;
	get_close_keypad = false;

	is_close_keycard = false;
	get_close_keycard = false;

	is_close_lockbox = false;
	get_close_lockbox = false;

	is_open_coffin = false;
	get_open_coffin = false;

	is_open_board = false;
	get_open_board = false;

	is_open_safedoor = false;
	get_open_safedoor = false;

	get_robot = false;
	set_visible_robot = false;

	get_baby = false;
	set_visible_baby = false;

	is_open_escape = false;
	get_open_escape = false;

	getlvbox_x = 1631.0;
	getlvbox_y = 1110.0;
	getlvbox_z = -630.0;

	key_setok = false;

	obj_robot_open = false;

	player_id = cplayer_id;
}

void ASPLevelScriptActor::initkey()
{
	if (key_init)
	{
		key_init = false;

		lv1f_x = firstf_x;
		lv1f_y = firstf_y;
		lv1f_z = firstf_z;

		lvbf_x = bf_x;
		lvbf_y = bf_y;
		lvbf_z = bf_z;

		lv2f_x = secf_x;
		lv2f_y = secf_y;
		lv2f_z = secf_z;

		key_setok = true;

	}
}

void ASPLevelScriptActor:: setIsopendoor()
{
	for (int i = 0; i < 21; ++i)
	{
		cis_close_door[i] = is_close_door[i];
	}

	cis_close_keypad = is_close_keypad;
	cis_close_keycard = is_close_keycard;
	cis_open_escape = is_open_escape;

		if (cis_push == true)
	{
		l2cb_push = true;
		cis_push = false;
	}
}

void ASPLevelScriptActor::setIsopencloset()
{
	for (int i = 0; i < 6; ++i)
	{
		cis_close_closet_left[i] = is_close_closet_left[i];
		cis_close_closet_right[i] = is_close_closet_right[i];
	}


	if (cis_closet_push == true)
	{
		l2cb_push = true;
		cis_closet_push = false;
	}
}

void ASPLevelScriptActor::setIsopendrawer()
{
	for (int i = 0; i < 8; ++i)
	{
		cis_close_drawer[i] = is_close_drawer[i];
	}


	if (cis_drawer_push == true)
	{
		l2cb_push = true;
		cis_drawer_push = false;
	}
}

void ASPLevelScriptActor::setIsopenrefriger()
{
	
	for (int i = 0; i < 4; ++i)
	{
		cis_close_refriger[i] = is_close_refriger[i];
	}


	if (cis_refriger_push == true)
	{
		l2cb_push = true;
		cis_refriger_push = false;
	}
}

void ASPLevelScriptActor::setIsopendryer()
{

	for (int i = 0; i < 2; ++i)
	{
		cis_close_dryer[i] = is_close_dryer[i];
	}

	cis_close_lockbox = is_close_lockbox;


	if (cis_dryer_push == true)
	{
		l2cb_push = true;
		cis_dryer_push = false;
	}
}

void ASPLevelScriptActor::setIsopenotherobject()
{

	cis_open_coffin = is_open_coffin;
	cget_robot = get_robot;
	cget_baby = get_baby;
	cis_open_board = is_open_board;
	cis_open_safedoor = is_open_safedoor;

	if (cotherobject_open || obj_robot_open)
	{
		otherobject_push = true;
		cotherobject_open = false;
		obj_robot_open = false;
	}
}

void ASPLevelScriptActor::set_movebox()
{
	if (cbox_move)
	{
		box_x = lvbox_x;
		box_y = lvbox_y;
		box_z = lvbox_z;

		box_push = true;
		cbox_move = false;
	}
}

void ASPLevelScriptActor::set_moveai()
{
	//if (cAI_move)
	//{
		for (int i = 0; i < 2; ++i)
		{
			mug_x[i] = lvmug_x[i];
			mug_y[i] = lvmug_y[i];
			mug_z[i] = lvmug_z[i];
			
			mug_rot_x[i] = lvmug_rot_x[i];
			mug_rot_y[i] = lvmug_rot_y[i];
			mug_rot_z[i] = lvmug_rot_z[i];

			mug_vx[i] = lvmug_vx[i];
			mug_vy[i] = lvmug_vy[i];
			mug_vz[i] = lvmug_vz[i];
		}


		ai_push = true;
		//cAI_move = false;
	//}
}

void ASPLevelScriptActor::set_escapekey()
{

	for (int i = 0; i < 3; ++i)
	{
		cget_escapekey[i] = get_escapekey[i];
	}



	ckey_push = true;

}

void ASPLevelScriptActor::set_objectkey()
{

	for (int i = 0; i < 11; ++i)
	{
		cget_objectkey[i] = get_objectkey[i];
	}

	ckey_push = true;



}

void ASPLevelScriptActor::c2s_Isopendoor()
{
	for (int i = 0; i < 21; ++i)
	{
		get_close_door[i] = getis_close_door[i];
	}

	for (int i = 0; i < 6; ++i)
	{
		get_close_closet_left[i] = getis_close_closet_left[i];
		get_close_closet_right[i] = getis_close_closet_right[i];
	}

	for (int i = 0; i < 8; ++i)
	{
		get_close_drawer[i] = getis_close_drawer[i];
	}

	for (int i = 0; i < 4; ++i)
	{
		get_close_refriger[i] = getis_close_refriger[i];
	}

	for (int i = 0; i < 2; ++i)
	{
		get_close_dryer[i] = getis_close_dryer[i];
	}

	get_close_lockbox = getis_close_lockbox;
	get_close_keypad = cget_close_keypad;
	get_close_keycard = cget_close_keycard;
	get_open_escape = cget_open_escape;

}

void ASPLevelScriptActor::c2s_key_visible()
{
	for (int i = 0; i < 3; ++i)
	{
		set_visible_escapekey[i] = s2c_get_escapekey[i];
	}

	for (int i = 0; i < 11; ++i)
	{
		set_visible_objectkey[i] = s2c_get_objectkey[i];
	}

}

void ASPLevelScriptActor::c2s_otherobject()
{

	get_open_coffin = cget_open_coffin;
	set_visible_robot = cset_visible_robot;
	set_visible_baby = cset_visible_baby;
	get_open_board = cget_open_board;
	get_open_safedoor = cget_open_safedoor;

}

void ASPLevelScriptActor::c2s_boxmove()
{

		getlvbox_x = getbox_x;
		getlvbox_y = getbox_y;
		getlvbox_z = getbox_z;

}

void ASPLevelScriptActor::c2s_aimove()
{

	for (int i = 0; i < 2; ++i)
	{
		getlvmug_x[i] = getmug_x[i];
		getlvmug_y[i] = getmug_y[i];
		getlvmug_z[i] = getmug_z[i];

		getlvmug_yaw[i] = getmug_yaw[i];
		getlvmug_pitch[i] = getmug_pitch[i];
		getlvmug_roll[i] = getmug_roll[i];

		getlvmug_vx[i] = getmug_vx[i];
		getlvmug_vy[i] = getmug_vy[i];
		getlvmug_vz[i] = getmug_vz[i];
	}
}


void ASPLevelScriptActor::set_levelpush()
{
	if (cs2c_door_push == true)
	{
		level_push = true;
		cs2c_door_push = false;
	}
}

void ASPLevelScriptActor::set_otherlevelpush()
{
	if (cs2c_otherobject_push == true)
	{
		otherlevel_push = true;
		cs2c_otherobject_push = false;
	}
}

void ASPLevelScriptActor::set_boxpush()
{
	if (cs2c_box_push == true)
	{
		lvbox_push = true;
		cs2c_box_push = false;
	}
}

void ASPLevelScriptActor::set_aipush()
{
	if (cs2c_ai_push == true)
	{
		lvai_push = true;
		cs2c_ai_push = false;
	}
}

void ASPLevelScriptActor::set_levelkey()
{
	if (cs2c_key_visible == true)
	{
		key_push = true;
		cs2c_key_visible = false;
	}
}
