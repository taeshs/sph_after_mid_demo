// Fill out your copyright notice in the Description page of Project Settings.


#include "MyCharacter.h"

// Sets default values
AMyCharacter::AMyCharacter()
{
	
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	/*
	static ConstructorHelpers::FObjectFinder<UBlueprint> Spawnchar(TEXT("/Game/ThirdPersonBP/Blueprints/test"));
	if (Spawnchar.Succeeded() && Spawnchar.Object) {
		UE_LOG(LogTemp, Warning, TEXT("-------------Spawnchar.Succeeded------------"));
		charBP = Spawnchar.Object;
	}

	//UWorld* const world = GetWorld();

	//AActor* spawnchar = GetWorld()->SpawnActor(charBP->GeneratedClass);
	//AActor* spawnchar = world->SpawnActor(charBP->GeneratedClass);
	//spawnchar->SetActorLocation(FVector(-1620.0f, -29.0f, 117.0f));
	*/

	if(bIsConnected)
		UE_LOG(LogClass, Log, TEXT("character oo!"));
}

// Called when the game starts or when spawned
void AMyCharacter::BeginPlay()
{
	Super::BeginPlay();

	tested = false;
}

void AMyCharacter::testchar()
{
	tested = true;
}

// Called every frame
void AMyCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);


}



// Called to bind functionality to input
void AMyCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

