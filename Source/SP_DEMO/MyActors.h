// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ClientSocket.h"
#include "GameFramework/Actor.h"
#include "MyActors.generated.h"

UCLASS()
class SP_DEMO_API AMyActors : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AMyActors();
	void UpdateNewPlayer(int id);
	void UpdateNewMonster(int id);
	void actormove();
	void Npcmove();
	void actordead();

	UPROPERTY(EditAnywhere,BlueprintReadWrite, Category = "Spawning")
		TArray<bool> spawneed;
		//bool spawneed;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Spawning")
		TArray<bool> monsterspawneed;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Spawning")
		bool isactormove;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Spawning")
		bool isactordestroy = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Spawning")
		int SessionId;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Spawning")
		int moveId;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Spawning")
		int npcmoveId;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Spawning")
		int npctoplayerId;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Spawning")
		int deadId;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Spawning")
		TArray<float> actorx;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Spawning")
		TArray<float> actory;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Spawning")
		TArray<float> actorz;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Spawning")
		TArray<float> actoryaw;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Spawning")
		TArray<float> actorpitch;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Spawning")
		TArray<float> actorroll;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Spawning")
		TArray<float> actorvx;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Spawning")
		TArray<float> actorvy;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Spawning")
		TArray<float> actorvz;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Spawning")
		TArray<bool> actorflash;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Spawning")
		TArray<float> monsterx;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Spawning")
		TArray<float> monstery;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Spawning")
		TArray<float> monsterz;


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
