// Fill out your copyright notice in the Description page of Project Settings.


#include "IPContainer.h"

int cip1, cip2, cip3, cip4;

Fint4 GetContainedIp() {
	Fint4 ips;
	ips.a = cip1; ips.b = cip2; ips.c = cip3; ips.d = cip4;

	return ips;
}

// Sets default values
AIPContainer::AIPContainer()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

void AIPContainer::Is_SetIp()
{
	is_setip = true;
}



Fint4 AIPContainer::GetIP()
{
	Fint4 ips;
	ips.a = cip1; ips.b = cip2; ips.c = cip3; ips.d = cip4;

	return ips;
}

void AIPContainer::SetIP(int a, int b, int c, int d) 
{
	cip1 = a; cip2 = b; cip3 = c; cip4 = d;
}

/*
// Called when the game starts or when spawned
void AIPContainer::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AIPContainer::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}
*/
